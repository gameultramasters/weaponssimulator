using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "WeaponStatsData", menuName = "ScriptableObjects/WeaponSO", order = 1)]
public class WeaponSO : ScriptableObject
{
	public List<int> availableAddons;
	public string name;
	public float missMod;
	public float accuracy;
	public float reloadSpeed;
	public int damage;
	public int ammunition;
	public int weaponID;
}
