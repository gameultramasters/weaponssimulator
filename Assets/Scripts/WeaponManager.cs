﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum WeaponType {

	None,
	Pistols,
	MachineGuns,
	RocketLauncher,
	SniperRifles
}

public enum AddonType {

	None,
	Magazine,
	Skin,
	Color,
	Scope,
	Damage,
	UnderWeapon
}

public class WeaponManager : MonoBehaviour {

	public Dictionary<int, Addon> gameAddons = new Dictionary<int, Addon> ();
	public Dictionary<int, WeaponStats> playerWeaponSets = new Dictionary<int, WeaponStats> ();

	public List<WeaponStats> pistols = new List<WeaponStats>();
	public List<WeaponStats> machineGuns = new List<WeaponStats>();
	public List<WeaponStats> rocketLaunchers = new List<WeaponStats>();
	public List<WeaponStats> sniperRifles = new List<WeaponStats>();
	[HideInInspector]
	public List<WeaponStats> actualWeaponsList = new List<WeaponStats> ();

	[HideInInspector]
	public List<Addon> actualAddonList = new List<Addon> ();
	public List<Addon> tmpGameAddons = new List<Addon>(); 
	[HideInInspector]
	public List<int> tmpWeaponAddons = new List<int> ();
	[HideInInspector]
	public WeaponStats actualSelectedWeapon;
	[HideInInspector]
	public WeaponType actualSelectedWeaponType;

	[HideInInspector]
	public float tmpAccuracyMod, missMod, accuracy, reloadSpeed, tmpReloadSpeedMod, maxReload, maxAccuracy;
	//[HideInInspector]
	public int tmpDamageMod, tmpAmmunitionMod, maxAttack, maxAmmunition, damage, ammunition;

	//public Color tmpNewColor;
	//public Texture tmpNewSkin;

	List<Addon> pistolAddons = new List<Addon> ();
	List<Addon> machineGunAddons = new List<Addon> ();
	List<Addon> rockeLauncherAddons = new List<Addon> ();
	List<Addon> sniperRifleAddons = new List<Addon> ();


	/// <summary>
    /// WeaponManager is main class for application data management with coordination with WeaponManagerUI as visual layout controller 
    /// </summary>
	public static WeaponManager Instance {
		get {
			if (instance == null)
				instance = GameObject.FindObjectOfType<WeaponManager> ();

			return instance;
		}
	}

	static WeaponManager instance;

	/// <summary>
	/// Setting up available weapon addons list
	/// </summary>
	public void Awake() {
		
	//	SetUpAddonsList ();
	}

	/// <summary>
    /// Setting up type of weapon we want to save
    /// </summary>
    /// <param name="_weaponTypeIndex"></param>
	public void SetUpWeaponType(int _weaponTypeIndex) {

		WeaponManagerUI.Instance.ClearAddonsList ();
	//	WeaponManagerUI.ResetAddonButtonsColor ();
		actualSelectedWeaponType = (WeaponType)_weaponTypeIndex;
		SetUpActualWeaponsList (actualSelectedWeaponType);
		GetMaxFactorValues();
		SetUpActualAddonsList (actualSelectedWeaponType);
		WeaponManagerUI.Instance.ShowWeaponTypeList (actualSelectedWeaponType);
	}

	/// <summary>
	/// Setting up actual selected weapon 
	/// </summary>
	/// <param name="_weaponIndex"></param>
	public void SetUpActualWeapon(int _weaponIndex) {
		actualSelectedWeapon = actualWeaponsList [_weaponIndex];
		damage = actualSelectedWeapon.damage;
		reloadSpeed = actualSelectedWeapon.reloadSpeed;
		ammunition = actualSelectedWeapon.ammunition;
		accuracy = actualSelectedWeapon.accuracy;
		WeaponManagerUI.ResetAddonButtonsColor ();
		ClearTmpMods ();
		WeaponManagerUI.Instance.HideAddonStats ();
		WeaponManagerUI.Instance.ShowAddonsList ();
		WeaponManagerUI.Instance.ShowBaseWeaponStats ();
	}

	/// <summary>
	/// Setting up addon for weapon with information if player want to mount or unmount weapon part
	/// </summary>
	/// <param name="_addonID"></param>
	/// <param name="_remove"></param>
	public void SetUpAddon(int _addonID, bool _remove) {

		if (_remove) {
			tmpWeaponAddons.Remove (_addonID);
			RemoveTmpMods (_addonID);
		} else {
			tmpWeaponAddons.Add (_addonID);
			AddTmpMods (_addonID);
		}

		WeaponManagerUI.Instance.ShowAddonStats (_addonID);
	}

	/// <summary>
	/// Return AddonId of already mounted element on weapon type
	/// </summary>
	/// <param name="_addonType"></param>
	/// <returns></returns>
	public int GetAddonIDOfTypeOnWeapon(AddonType _addonType) {

		foreach (var addonID in tmpWeaponAddons) {
			if (gameAddons [addonID].addonType == _addonType) {
				return addonID;
			} 
		}

		return -1;
	}

	/// <summary>
	/// Add selected addon stats to weapon
	/// </summary>
	public void AddAddonToWeapon() {
		//after save
		damage += tmpDamageMod;
		reloadSpeed += tmpReloadSpeedMod;
		accuracy += tmpAccuracyMod;
		ammunition += tmpAmmunitionMod;

		SaveLoadSystem.Instance.CreateWeaponSet ();

		ClearTmpMods ();
		tmpWeaponAddons.Clear ();

		WeaponManagerUI.Instance.HideAddonStats ();
		WeaponManagerUI.Instance.ShowBaseWeaponStats ();
	}

	/// <summary>
	/// Weapon data overrun protection
	/// </summary>
	public void GetMaxFactorValues() {
		foreach(WeaponStats weapon in actualWeaponsList) {

			if(weapon.accuracy > maxAccuracy) {
				maxAccuracy = weapon.accuracy;
			} 
			if(weapon.ammunition > maxAmmunition) {
				maxAmmunition = weapon.ammunition;
			}
			if(weapon.reloadSpeed > maxReload) {
				maxReload = weapon.reloadSpeed;
			}
			if(weapon.damage > maxAttack) {
				maxAttack = weapon.damage;
			}
		}

		maxAccuracy += maxAccuracy * 0.1f;
		maxAmmunition += (int)(maxAmmunition * 0.1f);
		maxReload += maxReload * 0.1f;
		maxAttack += (int)(maxAttack * 0.1f);
	}

	/// <summary>
	/// Clearing temporary data
	/// </summary>
	void ClearTmpMods() {
		tmpAccuracyMod = 0;
		tmpDamageMod = 0;
		tmpAmmunitionMod = 0;
		tmpReloadSpeedMod = 0;
	}

	/// <summary>
	/// Adding addon stats to temporary variables
	/// </summary>
	/// <param name="_addonID"></param>
	void AddTmpMods(int _addonID) {
		tmpAccuracyMod += gameAddons [_addonID].accuracyMod;
		tmpDamageMod += gameAddons [_addonID].damageMod;
		tmpAmmunitionMod += gameAddons [_addonID].ammunitionMod;
		tmpReloadSpeedMod += gameAddons [_addonID].reloadSpeedMod;
	}

	/// <summary>
	/// Removing addon stats from temporary variables
	/// </summary>
	/// <param name="_addonID"></param>
	void RemoveTmpMods(int _addonID) {
		tmpAccuracyMod -= gameAddons [_addonID].accuracyMod;
		tmpDamageMod -= gameAddons [_addonID].damageMod;
		tmpAmmunitionMod -= gameAddons [_addonID].ammunitionMod;
		tmpReloadSpeedMod -= gameAddons [_addonID].reloadSpeedMod;
	}

	/// <summary>
	/// Seting up available weapons for selected weapon type
	/// </summary>
	/// <param name="_weaponType"></param>
	void SetUpActualWeaponsList(WeaponType _weaponType) {

		actualWeaponsList.Clear ();

		switch (_weaponType) {
		case WeaponType.Pistols:
			for (int i = 0; i < pistols.Count; i++) {
				actualWeaponsList.Add (pistols [i]);
			}
			break;
		case WeaponType.MachineGuns:
			for (int i = 0; i < machineGuns.Count; i++) {
				actualWeaponsList.Add (machineGuns [i]);
			}
			break;
		case WeaponType.RocketLauncher:
			for (int i = 0; i < rocketLaunchers.Count; i++) {
				actualWeaponsList.Add (rocketLaunchers [i]);
			}
			break;
		case WeaponType.SniperRifles:
			for (int i = 0; i < sniperRifles.Count; i++) {
				actualWeaponsList.Add (sniperRifles [i]);
			}
			break;
		default:
			break;
		}
	}

	/// <summary>
	/// Seting up available addons for selected weapon type
	/// </summary>
	/// <param name="_weaponType"></param>
	void SetUpActualAddonsList(WeaponType _weaponType) {

		actualAddonList.Clear ();

		switch (_weaponType) {
		case WeaponType.Pistols:
			for (int i = 0; i < pistolAddons.Count; i++) {
				actualAddonList.Add (pistolAddons [i]);
			}
			break;
		case WeaponType.MachineGuns:
			for (int i = 0; i < machineGunAddons.Count; i++) {
				actualAddonList.Add (machineGunAddons [i]);
			}
			break;
		case WeaponType.RocketLauncher:
			for (int i = 0; i < rockeLauncherAddons.Count; i++) {
				actualAddonList.Add (rockeLauncherAddons [i]);
			}
			break;
		case WeaponType.SniperRifles:
			for (int i = 0; i < sniperRifleAddons.Count; i++) {
				actualAddonList.Add (sniperRifleAddons [i]);
			}
			break;
		default:
			break;
		}
	}

	void SetUpAddonsList() {

		for (int i = 0; i < tmpGameAddons.Count; i++) {
			gameAddons [tmpGameAddons [i].addonID] = tmpGameAddons [i];
		}

		foreach (var addon in gameAddons) {
			for (int i = 0; i < addon.Value.availableForWeaponsType.Length; i++) {
				
				switch ((WeaponType)addon.Value.availableForWeaponsType[i]) {
				case WeaponType.Pistols:
					pistolAddons.Add (addon.Value);
					break;
				case WeaponType.MachineGuns:
					machineGunAddons.Add (addon.Value);
					break;
				case WeaponType.RocketLauncher:
					rockeLauncherAddons.Add (addon.Value);
					break;
				case WeaponType.SniperRifles:
					sniperRifleAddons.Add (addon.Value);
					break;
				default:
					break;
				}
					
			}
		}
	}

}
