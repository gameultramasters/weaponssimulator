﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum ButtonType {
	None,
	Addon,
	Weapon,
	WeaponType,
	WeaponSet
}

public class WeaponManagerUI : MonoBehaviour, IMenu, IWeaponManagerUI {

	public static System.Action ResetAddonButtonsColor;

	public Dictionary<int, GameObject> activeAddonButtonsByID = new Dictionary<int, GameObject>();

	public GameObject menuPanel;
	public GameObject weaponManagerPanel;
	public GameObject weaponsListContent;
	public GameObject addonsListContent;

	public GameObject atkBaseBar;
	public GameObject atkPositiveModBar;
	public GameObject atkNegativeModBar;


	public GameObject rldBaseBar;
	public GameObject rldPositiveModBar;
	public GameObject rldNegativeModBar;


	public GameObject ammoBaseBar;
	public GameObject ammoPositiveModBar;
	public GameObject ammoNegativeModBar;


	public GameObject accBaseBar;
	public GameObject accPositiveModBar;
	public GameObject accNegativeModBar;


	public Color buttonSelectionColor;

	public GameObject ActualPanel {
		get	{
			return actualPanel;
		} set {
			actualPanel = value;
		}
	}

	GameObject actualPanel;

	public static WeaponManagerUI Instance {
		get {
			if (instance == null)
				instance = GameObject.FindObjectOfType<WeaponManagerUI> ();

			return instance;
		}
	}

	static WeaponManagerUI instance;

	public void ShowWeaponTypeList (WeaponType _weaponType){

		for (int i = 0; i < weaponsListContent.transform.childCount; i++) {
			if (i < WeaponManager.Instance.actualWeaponsList.Count) {
				weaponsListContent.transform.GetChild (i).GetChild (0).GetComponent<Text> ().text = WeaponManager.Instance.actualWeaponsList [i].name;
				weaponsListContent.transform.GetChild (i).gameObject.SetActive (true);
			} else {
				weaponsListContent.transform.GetChild (i).gameObject.SetActive (false);
			}
		}
		
	}
		
	public void ShowAddonsList (){

		for (int i = 0; i < addonsListContent.transform.childCount; i++) {
			if (i < WeaponManager.Instance.actualAddonList.Count) {
				for (int j = 0; j < WeaponManager.Instance.actualAddonList[i].availableForWeaponsID.Length; j++) {
					if (WeaponManager.Instance.actualAddonList [i].availableForWeaponsID [j] == WeaponManager.Instance.actualSelectedWeapon.weaponID) {
						addonsListContent.transform.GetChild (i).GetChild (0).GetComponent<Text> ().text = WeaponManager.Instance.actualAddonList [i].name;
						addonsListContent.transform.GetChild (i).GetComponent<ButtonSystem> ().elementID = WeaponManager.Instance.actualAddonList [i].addonID;
						addonsListContent.transform.GetChild (i).gameObject.SetActive (true);
						break;
					} else {
						addonsListContent.transform.GetChild (i).gameObject.SetActive (false);
					}
				}
			} else {
				addonsListContent.transform.GetChild (i).gameObject.SetActive (false);
			}
		}
	}

	public void ClearAddonsList() {
		for (int i = 0; i < addonsListContent.transform.childCount; i++) {
			addonsListContent.transform.GetChild (i).gameObject.SetActive (false);
		}
	}

	float atkBaseBarSize;
	float accBaseBarSize;
	float ammoBaseBarSize;
	float rldBaseBarSize;
	public void ShowBaseWeaponStats() {
		atkBaseBarSize = (float)WeaponManager.Instance.damage / (float)WeaponManager.Instance.maxAttack;
		atkBaseBar.GetComponent<RectTransform>().localScale = new Vector3(atkBaseBarSize, 1,1);
		rldBaseBarSize = WeaponManager.Instance.reloadSpeed / WeaponManager.Instance.maxReload;
		rldBaseBar.GetComponent<RectTransform>().localScale = new Vector3(rldBaseBarSize, 1,1);
		ammoBaseBarSize = (float)WeaponManager.Instance.ammunition / (float)WeaponManager.Instance.maxAmmunition;
		ammoBaseBar.GetComponent<RectTransform>().localScale = new Vector3(ammoBaseBarSize, 1,1);
		accBaseBarSize = WeaponManager.Instance.accuracy / WeaponManager.Instance.maxAccuracy;
		accBaseBar.GetComponent<RectTransform>().localScale = new Vector3(accBaseBarSize, 1,1);
	}

	public void ShowAddonStats(int _addonID) {

		float tmpModificatorSize = 0;

		if(WeaponManager.Instance.tmpDamageMod > 0) {
			tmpModificatorSize = (float)WeaponManager.Instance.tmpDamageMod / (float)WeaponManager.Instance.maxAttack;
			if(atkBaseBarSize + tmpModificatorSize > 1)
				tmpModificatorSize = 1 - atkBaseBarSize;
			atkPositiveModBar.GetComponent<RectTransform>().localScale = new Vector3(atkBaseBarSize + tmpModificatorSize,1,1);
			atkPositiveModBar.SetActive(true);
		} else if ( WeaponManager.Instance.tmpDamageMod < 0) {
			//atkNegativeModBar.SetActive(true);
		} else {
			atkPositiveModBar.SetActive(false);
			atkNegativeModBar.SetActive(false);
		}

		if(WeaponManager.Instance.tmpReloadSpeedMod > 0) {
			tmpModificatorSize = WeaponManager.Instance.tmpReloadSpeedMod / WeaponManager.Instance.maxReload;
			if(rldBaseBarSize + tmpModificatorSize > 1)
				tmpModificatorSize = 1 - rldBaseBarSize;
			rldPositiveModBar.GetComponent<RectTransform>().localScale = new Vector3(rldBaseBarSize + tmpModificatorSize,1,1);
			rldPositiveModBar.SetActive(true);
		} else if ( WeaponManager.Instance.tmpReloadSpeedMod < 0) {
			//rldNegativeModBar.SetActive(true);
		} else {
			rldPositiveModBar.SetActive(false);
			rldNegativeModBar.SetActive(false);
		}

		if(WeaponManager.Instance.tmpAmmunitionMod > 0) {
			tmpModificatorSize = (float)WeaponManager.Instance.tmpAmmunitionMod / (float)WeaponManager.Instance.maxAmmunition;
			if(ammoBaseBarSize + tmpModificatorSize > 1)
				tmpModificatorSize = 1 - ammoBaseBarSize;
			ammoPositiveModBar.GetComponent<RectTransform>().localScale = new Vector3(ammoBaseBarSize + tmpModificatorSize,1,1);
			ammoPositiveModBar.SetActive(true);
		} else if ( WeaponManager.Instance.tmpAmmunitionMod < 0) {
			//ammoNegativeModBar.SetActive(true);
		} else {
			ammoPositiveModBar.SetActive(false);
			ammoNegativeModBar.SetActive(false);
		}

		if(WeaponManager.Instance.tmpAccuracyMod > 0) {
			tmpModificatorSize = WeaponManager.Instance.tmpAccuracyMod / WeaponManager.Instance.maxAccuracy;
			if(accBaseBarSize + tmpModificatorSize > 1)
				tmpModificatorSize = 1 - accBaseBarSize;
			accPositiveModBar.GetComponent<RectTransform>().localScale = new Vector3(accBaseBarSize + tmpModificatorSize,1,1);
			accPositiveModBar.SetActive(true);
		} else if ( WeaponManager.Instance.tmpAccuracyMod < 0) {
			//accNegativeModBar.SetActive(true);
		} else {
			accPositiveModBar.SetActive(false);
			accNegativeModBar.SetActive(false);
		}

		// atkModValue.text = WeaponManager.Instance.tmpDamageMod.ToString();
		// rldModValue.text = WeaponManager.Instance.tmpReloadSpeedMod.ToString();
		// ammoModValue.text = WeaponManager.Instance.tmpAmmunitionMod.ToString();
		// accModValue.text = WeaponManager.Instance.tmpAccuracyMod.ToString();

		// atkModValue.gameObject.SetActive (true);
		// rldModValue.gameObject.SetActive (true);
		// ammoModValue.gameObject.SetActive (true);
		// accModValue.gameObject.SetActive (true);
	}

	public void HideAddonStats() {
		// atkModValue.gameObject.SetActive (false);
		// rldModValue.gameObject.SetActive (false);
		// ammoModValue.gameObject.SetActive (false);
		// accModValue.gameObject.SetActive (false);
	}

	public void BackToMenu(){}

	public void CloseUIPanel(){
		ActualPanel.SetActive (false);
	}
		
}
