﻿using UnityEngine;

[System.Serializable]
public struct Addon {

	public int[] availableForWeaponsType;
	public int[] availableForWeaponsID;
	public int addonID;
	public string name;
	public float missMod;
	public float accuracyMod;
	public int damageMod;
	public int ammunitionMod;
	public int reloadSpeedMod;
	public Color newColor;
	public Texture newSkin;
	public AddonType addonType;
}
