﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct WeaponStats {

	public List<int> addons;
	public string name;
	public float missMod;
	public float accuracy;
	public float reloadSpeed;
	public int damage;
	public int ammunition;
	public int weaponID;
}
