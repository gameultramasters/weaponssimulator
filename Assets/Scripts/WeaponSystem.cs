﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponSystem : MonoBehaviour, IWeaponUse, IWeaponVisual {

	public float missMod;
	public float accuracy;
	public int damage;
	public int ammunition;
	public int actualAmmo;

	public void ReloadWeapon() {

		actualAmmo = ammunition;
	}
	public virtual void Shot (){}
	public virtual void ChangeFireType (){}
	public void Aim (){}


	public void ChangeWeaponColor (){}
	public void ChangeWeaponSkin(){}
	public void SetUpWeaponMainLook (){}

}
