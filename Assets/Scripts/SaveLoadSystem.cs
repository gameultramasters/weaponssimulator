﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveLoadSystem : MonoBehaviour {

	public SDictionary weaponSets = new SDictionary();
	Serializer serializer = new Serializer();

	public static SaveLoadSystem Instance {
		get {
			if (instance == null)
				instance = GameObject.FindObjectOfType<SaveLoadSystem> ();

			return instance;
		}
	}

	static SaveLoadSystem instance;

	public void Awake() {

		LoadWeapons ();
		//serializer.Delete("weaponSets");
	}
		
	public void CreateWeaponSet() {
		WeaponStats weaponStatsToSave = new WeaponStats ();
		if(weaponSets == null) {
			weaponSets = new SDictionary();
		}
		weaponStatsToSave.name = "Weapon Set" + weaponSets.ValuesList.Count;
		weaponStatsToSave.accuracy = WeaponManager.Instance.accuracy;
		weaponStatsToSave.damage = WeaponManager.Instance.damage;
		weaponStatsToSave.reloadSpeed = WeaponManager.Instance.reloadSpeed;
		weaponStatsToSave.ammunition = WeaponManager.Instance.ammunition;
		weaponStatsToSave.addons = new List<int> ();
		for (int i = 0; i < WeaponManager.Instance.tmpWeaponAddons.Count; i++) {
			weaponStatsToSave.addons.Add (WeaponManager.Instance.tmpWeaponAddons [i]);
		}

		weaponSets.Add (weaponSets.ValuesList.Count, weaponStatsToSave);
		Debug.Log("Weapon Create");

		SaveWeapons ();
	}

	public void SaveWeapons() {
		Debug.Log("Weapon Save");
		serializer.Save<SDictionary> ("weaponSets", weaponSets);
	}

	void LoadWeapons() {
		weaponSets = serializer.Load<SDictionary> ("weaponSets");
	}
}
