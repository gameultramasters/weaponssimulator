﻿using UnityEngine;
using UnityEngine.UI;

public class ButtonSystem : MonoBehaviour {


	public ButtonType buttonType;
	public int elementID;
	public bool buttonIsSelected;

	Color defaultButtonColor;

	bool executeOnReset;
	int tmpAnotherAddonID;

	void Start() {
		gameObject.SetActive (false);
		defaultButtonColor = gameObject.GetComponent<Image> ().color;

		if (buttonType == ButtonType.Addon) {
			WeaponManagerUI.ResetAddonButtonsColor += ResetButtonColor;
		}
	}

	void OnDestroy() {
		if (buttonType == ButtonType.Addon) {
			WeaponManagerUI.ResetAddonButtonsColor -= ResetButtonColor;
		}
	}

	public void OnClick() {

		if (!buttonIsSelected) {
			if (buttonType == ButtonType.Addon) {
				tmpAnotherAddonID = WeaponManager.Instance.GetAddonIDOfTypeOnWeapon (WeaponManager.Instance.gameAddons [elementID].addonType);
				if (tmpAnotherAddonID >= 0) {
					SetUpAddon (tmpAnotherAddonID, true);
				}
				SetUpAddon (elementID, false);
			}
		} else {

			if (buttonType == ButtonType.Addon) {
				SetUpAddon (elementID, true);
			}
		}
	}

	public void ResetButtonColor() {
	//	buttonIsSelected = false;
		executeOnReset = true;
		DeactivateButton(elementID);
	//	gameObject.GetComponent<Image> ().color = defaultButtonColor;
	}

	void SetUpAddon(int _addonID, bool _remove) {
		//WeaponManager.Instance.SetUpAddon (_addonID, _remove);
		if (_remove) {
			DeactivateButton (_addonID);
		} else {
			ActivateButton (_addonID);
		}
		executeOnReset = false;
	}

	void ActivateButton(int _elementID) {

				WeaponManager.Instance.SetUpAddon (_elementID, false);

		WeaponManagerUI.Instance.activeAddonButtonsByID.Add (_elementID, gameObject);
		WeaponManagerUI.Instance.activeAddonButtonsByID [_elementID].GetComponent<ButtonSystem> ().buttonIsSelected = true;
		WeaponManagerUI.Instance.activeAddonButtonsByID [_elementID].GetComponent<Image> ().color = WeaponManagerUI.Instance.buttonSelectionColor;
		//gameObject.GetComponent<Image> ().color = WeaponManagerUI.Instance.buttonSelectionColor;
	}

	void DeactivateButton(int _elementID) {
		if(WeaponManagerUI.Instance.activeAddonButtonsByID.ContainsKey(_elementID)) {
			//if(executeOnReset)
				WeaponManager.Instance.SetUpAddon (_elementID, true);
			WeaponManagerUI.Instance.activeAddonButtonsByID [_elementID].GetComponent<ButtonSystem> ().buttonIsSelected = false;
			WeaponManagerUI.Instance.activeAddonButtonsByID [_elementID].GetComponent<Image> ().color = defaultButtonColor;
			WeaponManagerUI.Instance.activeAddonButtonsByID.Remove (_elementID);
		}

		//gameObject.GetComponent<Image> ().color = defaultButtonColor;
	}
}
