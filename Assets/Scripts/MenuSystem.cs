﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MenuPanel {
	MainMenu = 0,
	Options = 1,
	Credits = 2,
	WeaponEditor = 3,
	WeaponSets = 4
}

public class MenuSystem : MonoBehaviour, IMenu
{

	public static MenuSystem Instance{
		get {
			if (instance == null)
				instance = GameObject.FindObjectOfType<MenuSystem> ();

			return instance;
		}
	}

	static MenuSystem instance;

	public GameObject[] menuPanels;

	public GameObject ActualPanel {
		get	{
			return actualPanel;
		} set {
			actualPanel = value;
		}
	}

	GameObject actualPanel;

	void Start() {

		ActualPanel = menuPanels[(int)MenuPanel.MainMenu];
	}

	public void ChangePanel(int _panelIndex) {

		HidePanel (ActualPanel);
		ActualPanel = menuPanels[_panelIndex];
		ShowPanel (ActualPanel);
	}

	public void BackToMenu() {
		ChangePanel ((int)MenuPanel.MainMenu);
	}

	public void ExitGame() {

		Application.Quit ();
	}

	void ShowPanel(GameObject _panel) {

		_panel.SetActive (true);
	}

	void HidePanel(GameObject _panel) {
		_panel.SetActive (false);
	}
}
