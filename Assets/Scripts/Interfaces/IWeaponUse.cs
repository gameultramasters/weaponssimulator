﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IWeaponUse {

	void ReloadWeapon ();
	void Shot ();
	void ChangeFireType ();
	void Aim ();
}
