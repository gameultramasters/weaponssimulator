﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IWeaponManagerUI {

	void ShowWeaponTypeList (WeaponType _weaponType);
	void ShowAddonsList ();
}
