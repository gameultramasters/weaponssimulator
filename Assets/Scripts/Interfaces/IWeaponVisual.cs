﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IWeaponVisual {

	void ChangeWeaponColor ();
	void ChangeWeaponSkin();
	void SetUpWeaponMainLook ();
}
